const express = require("express");
const bodyParser = require("body-parser");
const graphQlHttp = require("express-graphql");
const mongoose = require("mongoose");

const graphQlSchema = require('./graphql/schema');
const graphQlResolvers = require('./graphql/resolvers');

const isAuth = require('./middleware/is-auth');

// Create app
const app = express();

// Middlewares
app.use(bodyParser.json());
app.use(isAuth);

// Setup graphql route
app.use(
  "/graph",
  graphQlHttp({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: true,
  })
);

// Connect to mongo
mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0-ynzmz.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`
  )
  .then(() => {
    // Start app
    app.listen(3000);
  })
  .catch((err) => {
    console.log(err);
  });
