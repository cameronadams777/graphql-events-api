const bcrypt = require("bcryptjs");
const User = require("../../models/user");
const jwt = require("jsonwebtoken");

module.exports = {
  createUser: async (args) => {
    try {
      const existingUser = await User.findOne({ email: args.userInput.email });
      if (existingUser) throw new Error("User exists already.");
      const hashedPassword = await bcrypt.hash(args.userInput.password, 16);
      const user = new User({
        email: args.userInput.email,
        password: hashedPassword,
      });
      const result = await user.save();
      return { ...result._doc, password: null, _id: result.id };
    } catch (error) {
      throw error;
    }
  },
  login: async ({ email, password }) => {
    const user = await User.findOne({ email });
    if (!user) throw new Error("User does not exist.");
    
    const doesPasswordMatch = await bcrypt.compare(password, user.password);
    if (!doesPasswordMatch) throw new Error("Invalid credentials.");
    
    const token = jwt.sign({ userId: user.id, email: user.email }, process.env.JWT_SECRET_KEY, {
      expiresIn: '1h'
    });

    return {
      userId: user.id,
      tokenExpiration: 1,
      token
    }
  },
};
