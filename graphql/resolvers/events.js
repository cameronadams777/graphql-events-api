const Event = require("../../models/event");
const User = require("../../models/user");
const { transformEvent } = require("./utils/merge");

module.exports = {
  events: async () => {
    try {
      const events = await Event.find();
      return events.map((event) => {
        return transformEvent(event);
      });
    } catch (error) {
      throw error;
    }
  },
  createEvent: async (args, req) => {
    try {
      if (!req.isAuth) {
        throw new Error("Unauthenticated!");
      }
      const event = new Event({
        title: args.eventInput.title,
        description: args.eventInput.description,
        price: +args.eventInput.price,
        date: new Date(args.eventInput.date),
        creator: "5eef896d6a195291693db531",
      });
      const result = await event.save();
      const createdEvent = transformEvent(result);
      const creator = await User.findById("5eef896d6a195291693db531");
      if (!creator) {
        throw new Error("User not found.");
      }
      await creator.createdEvents.push(event);
      await creator.save();
      return createdEvent;
    } catch (error) {
      throw error;
    }
  },
};
